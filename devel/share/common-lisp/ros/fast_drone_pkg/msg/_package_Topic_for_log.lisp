(cl:in-package fast_drone_pkg-msg)
(cl:export '(HEADER-VAL
          HEADER
          TIME-VAL
          TIME
          DRONE_STATE-VAL
          DRONE_STATE
          CONTROL_COMMAND-VAL
          CONTROL_COMMAND
          ATTITUDE_REFERENCE-VAL
          ATTITUDE_REFERENCE
          CONTROL_OUTPUT-VAL
          CONTROL_OUTPUT
))