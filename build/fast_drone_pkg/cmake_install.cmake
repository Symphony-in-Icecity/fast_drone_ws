# Install script for directory: /home/dqn/fast_drone_ws/src/fast_drone_pkg

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/dqn/fast_drone_ws/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/fast_drone_pkg/msg" TYPE FILE FILES
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg"
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg"
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg"
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg"
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg"
    "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/fast_drone_pkg/cmake" TYPE FILE FILES "/home/dqn/fast_drone_ws/build/fast_drone_pkg/catkin_generated/installspace/fast_drone_pkg-msg-paths.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/devel/include/fast_drone_pkg")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/roseus/ros" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/devel/share/roseus/ros/fast_drone_pkg")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/common-lisp/ros" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/devel/share/common-lisp/ros/fast_drone_pkg")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/gennodejs/ros" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/devel/share/gennodejs/ros/fast_drone_pkg")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  execute_process(COMMAND "/usr/bin/python2" -m compileall "/home/dqn/fast_drone_ws/devel/lib/python2.7/dist-packages/fast_drone_pkg")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/python2.7/dist-packages" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/devel/lib/python2.7/dist-packages/fast_drone_pkg")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig" TYPE FILE FILES "/home/dqn/fast_drone_ws/build/fast_drone_pkg/catkin_generated/installspace/fast_drone_pkg.pc")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/fast_drone_pkg/cmake" TYPE FILE FILES "/home/dqn/fast_drone_ws/build/fast_drone_pkg/catkin_generated/installspace/fast_drone_pkg-msg-extras.cmake")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/fast_drone_pkg/cmake" TYPE FILE FILES
    "/home/dqn/fast_drone_ws/build/fast_drone_pkg/catkin_generated/installspace/fast_drone_pkgConfig.cmake"
    "/home/dqn/fast_drone_ws/build/fast_drone_pkg/catkin_generated/installspace/fast_drone_pkgConfig-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/fast_drone_pkg" TYPE FILE FILES "/home/dqn/fast_drone_ws/src/fast_drone_pkg/package.xml")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/fast_drone_pkg" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/src/fast_drone_pkg/include/fast_drone_pkg/" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/fast_drone_pkg/launch" TYPE DIRECTORY FILES "/home/dqn/fast_drone_ws/src/fast_drone_pkg/launch/")
endif()

