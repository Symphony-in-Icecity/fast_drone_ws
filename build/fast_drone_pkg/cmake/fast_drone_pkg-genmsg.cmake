# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "fast_drone_pkg: 7 messages, 0 services")

set(MSG_I_FLAGS "-Ifast_drone_pkg:/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg;-Igeometry_msgs:/opt/ros/kinetic/share/geometry_msgs/cmake/../msg;-Isensor_msgs:/opt/ros/kinetic/share/sensor_msgs/cmake/../msg;-Istd_msgs:/opt/ros/kinetic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(fast_drone_pkg_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" "geometry_msgs/Quaternion:std_msgs/Header"
)

get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" "std_msgs/Header:fast_drone_pkg/TrajectoryPoint"
)

get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" "geometry_msgs/Quaternion:std_msgs/Header"
)

get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" "std_msgs/Header"
)

get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" "std_msgs/Header:fast_drone_pkg/TrajectoryPoint"
)

get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" NAME_WE)
add_custom_target(_fast_drone_pkg_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "fast_drone_pkg" "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" "fast_drone_pkg/DroneState:fast_drone_pkg/ControlOutput:fast_drone_pkg/AttitudeReference:fast_drone_pkg/ControlCommand:geometry_msgs/Quaternion:fast_drone_pkg/TrajectoryPoint:std_msgs/Header"
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_cpp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg"
  "${MSG_I_FLAGS}"
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
)

### Generating Services

### Generating Module File
_generate_module_cpp(fast_drone_pkg
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(fast_drone_pkg_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(fast_drone_pkg_generate_messages fast_drone_pkg_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_cpp _fast_drone_pkg_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fast_drone_pkg_gencpp)
add_dependencies(fast_drone_pkg_gencpp fast_drone_pkg_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fast_drone_pkg_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_eus(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg"
  "${MSG_I_FLAGS}"
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
)

### Generating Services

### Generating Module File
_generate_module_eus(fast_drone_pkg
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(fast_drone_pkg_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(fast_drone_pkg_generate_messages fast_drone_pkg_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_eus _fast_drone_pkg_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fast_drone_pkg_geneus)
add_dependencies(fast_drone_pkg_geneus fast_drone_pkg_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fast_drone_pkg_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_lisp(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg"
  "${MSG_I_FLAGS}"
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
)

### Generating Services

### Generating Module File
_generate_module_lisp(fast_drone_pkg
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(fast_drone_pkg_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(fast_drone_pkg_generate_messages fast_drone_pkg_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_lisp _fast_drone_pkg_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fast_drone_pkg_genlisp)
add_dependencies(fast_drone_pkg_genlisp fast_drone_pkg_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fast_drone_pkg_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_nodejs(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg"
  "${MSG_I_FLAGS}"
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
)

### Generating Services

### Generating Module File
_generate_module_nodejs(fast_drone_pkg
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(fast_drone_pkg_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(fast_drone_pkg_generate_messages fast_drone_pkg_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_nodejs _fast_drone_pkg_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fast_drone_pkg_gennodejs)
add_dependencies(fast_drone_pkg_gennodejs fast_drone_pkg_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fast_drone_pkg_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg"
  "${MSG_I_FLAGS}"
  "/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)
_generate_msg_py(fast_drone_pkg
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg"
  "${MSG_I_FLAGS}"
  "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg/Quaternion.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg;/opt/ros/kinetic/share/std_msgs/cmake/../msg/Header.msg"
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
)

### Generating Services

### Generating Module File
_generate_module_py(fast_drone_pkg
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(fast_drone_pkg_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(fast_drone_pkg_generate_messages fast_drone_pkg_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg" NAME_WE)
add_dependencies(fast_drone_pkg_generate_messages_py _fast_drone_pkg_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(fast_drone_pkg_genpy)
add_dependencies(fast_drone_pkg_genpy fast_drone_pkg_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS fast_drone_pkg_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/fast_drone_pkg
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_cpp)
  add_dependencies(fast_drone_pkg_generate_messages_cpp geometry_msgs_generate_messages_cpp)
endif()
if(TARGET sensor_msgs_generate_messages_cpp)
  add_dependencies(fast_drone_pkg_generate_messages_cpp sensor_msgs_generate_messages_cpp)
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(fast_drone_pkg_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/fast_drone_pkg
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_eus)
  add_dependencies(fast_drone_pkg_generate_messages_eus geometry_msgs_generate_messages_eus)
endif()
if(TARGET sensor_msgs_generate_messages_eus)
  add_dependencies(fast_drone_pkg_generate_messages_eus sensor_msgs_generate_messages_eus)
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(fast_drone_pkg_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/fast_drone_pkg
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_lisp)
  add_dependencies(fast_drone_pkg_generate_messages_lisp geometry_msgs_generate_messages_lisp)
endif()
if(TARGET sensor_msgs_generate_messages_lisp)
  add_dependencies(fast_drone_pkg_generate_messages_lisp sensor_msgs_generate_messages_lisp)
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(fast_drone_pkg_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/fast_drone_pkg
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_nodejs)
  add_dependencies(fast_drone_pkg_generate_messages_nodejs geometry_msgs_generate_messages_nodejs)
endif()
if(TARGET sensor_msgs_generate_messages_nodejs)
  add_dependencies(fast_drone_pkg_generate_messages_nodejs sensor_msgs_generate_messages_nodejs)
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(fast_drone_pkg_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/fast_drone_pkg
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET geometry_msgs_generate_messages_py)
  add_dependencies(fast_drone_pkg_generate_messages_py geometry_msgs_generate_messages_py)
endif()
if(TARGET sensor_msgs_generate_messages_py)
  add_dependencies(fast_drone_pkg_generate_messages_py sensor_msgs_generate_messages_py)
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(fast_drone_pkg_generate_messages_py std_msgs_generate_messages_py)
endif()
