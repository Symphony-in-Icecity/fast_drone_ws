(cl:in-package fast_drone_pkg-msg)
(cl:export '(HEADER-VAL
          HEADER
          THROTTLE_SP-VAL
          THROTTLE_SP
          DESIRED_THROTTLE-VAL
          DESIRED_THROTTLE
          DESIRED_ATTITUDE-VAL
          DESIRED_ATTITUDE
          DESIRED_ATT_Q-VAL
          DESIRED_ATT_Q
))