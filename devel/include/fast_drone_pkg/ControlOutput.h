// Generated by gencpp from file fast_drone_pkg/ControlOutput.msg
// DO NOT EDIT!


#ifndef FAST_DRONE_PKG_MESSAGE_CONTROLOUTPUT_H
#define FAST_DRONE_PKG_MESSAGE_CONTROLOUTPUT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>

#include <std_msgs/Header.h>

namespace fast_drone_pkg
{
template <class ContainerAllocator>
struct ControlOutput_
{
  typedef ControlOutput_<ContainerAllocator> Type;

  ControlOutput_()
    : header()
    , u_l()
    , u_d()
    , NE()
    , v_cmd()
    , Thrust()
    , Throttle()  {
      u_l.assign(0.0);

      u_d.assign(0.0);

      NE.assign(0.0);

      v_cmd.assign(0.0);

      Thrust.assign(0.0);

      Throttle.assign(0.0);
  }
  ControlOutput_(const ContainerAllocator& _alloc)
    : header(_alloc)
    , u_l()
    , u_d()
    , NE()
    , v_cmd()
    , Thrust()
    , Throttle()  {
  (void)_alloc;
      u_l.assign(0.0);

      u_d.assign(0.0);

      NE.assign(0.0);

      v_cmd.assign(0.0);

      Thrust.assign(0.0);

      Throttle.assign(0.0);
  }



   typedef  ::std_msgs::Header_<ContainerAllocator>  _header_type;
  _header_type header;

   typedef boost::array<float, 3>  _u_l_type;
  _u_l_type u_l;

   typedef boost::array<float, 3>  _u_d_type;
  _u_d_type u_d;

   typedef boost::array<float, 3>  _NE_type;
  _NE_type NE;

   typedef boost::array<float, 3>  _v_cmd_type;
  _v_cmd_type v_cmd;

   typedef boost::array<float, 3>  _Thrust_type;
  _Thrust_type Thrust;

   typedef boost::array<float, 3>  _Throttle_type;
  _Throttle_type Throttle;





  typedef boost::shared_ptr< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> const> ConstPtr;

}; // struct ControlOutput_

typedef ::fast_drone_pkg::ControlOutput_<std::allocator<void> > ControlOutput;

typedef boost::shared_ptr< ::fast_drone_pkg::ControlOutput > ControlOutputPtr;
typedef boost::shared_ptr< ::fast_drone_pkg::ControlOutput const> ControlOutputConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::fast_drone_pkg::ControlOutput_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace fast_drone_pkg

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': True}
// {'fast_drone_pkg': ['/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg'], 'sensor_msgs': ['/opt/ros/kinetic/share/sensor_msgs/cmake/../msg'], 'geometry_msgs': ['/opt/ros/kinetic/share/geometry_msgs/cmake/../msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> const>
  : TrueType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
{
  static const char* value()
  {
    return "bcc482764830d65d3893dc2a6e9d19de";
  }

  static const char* value(const ::fast_drone_pkg::ControlOutput_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0xbcc482764830d65dULL;
  static const uint64_t static_value2 = 0x3893dc2a6e9d19deULL;
};

template<class ContainerAllocator>
struct DataType< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
{
  static const char* value()
  {
    return "fast_drone_pkg/ControlOutput";
  }

  static const char* value(const ::fast_drone_pkg::ControlOutput_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
{
  static const char* value()
  {
    return "std_msgs/Header header\n\
\n\
## 位置控制器输出： 期望角度、期望角加速度\n\
float32[3] u_l                 ## [0-1] 惯性系下的油门量\n\
float32[3] u_d                 ## [rad]\n\
\n\
float32[3] NE                  ## [rad]\n\
\n\
float32[3] v_cmd               ## [m/s]\n\
  \n\
float32[3] Thrust              ## [rad]  \n\
float32[3] Throttle\n\
================================================================================\n\
MSG: std_msgs/Header\n\
# Standard metadata for higher-level stamped data types.\n\
# This is generally used to communicate timestamped data \n\
# in a particular coordinate frame.\n\
# \n\
# sequence ID: consecutively increasing ID \n\
uint32 seq\n\
#Two-integer timestamp that is expressed as:\n\
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')\n\
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')\n\
# time-handling sugar is provided by the client library\n\
time stamp\n\
#Frame this data is associated with\n\
# 0: no frame\n\
# 1: global frame\n\
string frame_id\n\
";
  }

  static const char* value(const ::fast_drone_pkg::ControlOutput_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.header);
      stream.next(m.u_l);
      stream.next(m.u_d);
      stream.next(m.NE);
      stream.next(m.v_cmd);
      stream.next(m.Thrust);
      stream.next(m.Throttle);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct ControlOutput_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::fast_drone_pkg::ControlOutput_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::fast_drone_pkg::ControlOutput_<ContainerAllocator>& v)
  {
    s << indent << "header: ";
    s << std::endl;
    Printer< ::std_msgs::Header_<ContainerAllocator> >::stream(s, indent + "  ", v.header);
    s << indent << "u_l[]" << std::endl;
    for (size_t i = 0; i < v.u_l.size(); ++i)
    {
      s << indent << "  u_l[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.u_l[i]);
    }
    s << indent << "u_d[]" << std::endl;
    for (size_t i = 0; i < v.u_d.size(); ++i)
    {
      s << indent << "  u_d[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.u_d[i]);
    }
    s << indent << "NE[]" << std::endl;
    for (size_t i = 0; i < v.NE.size(); ++i)
    {
      s << indent << "  NE[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.NE[i]);
    }
    s << indent << "v_cmd[]" << std::endl;
    for (size_t i = 0; i < v.v_cmd.size(); ++i)
    {
      s << indent << "  v_cmd[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.v_cmd[i]);
    }
    s << indent << "Thrust[]" << std::endl;
    for (size_t i = 0; i < v.Thrust.size(); ++i)
    {
      s << indent << "  Thrust[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.Thrust[i]);
    }
    s << indent << "Throttle[]" << std::endl;
    for (size_t i = 0; i < v.Throttle.size(); ++i)
    {
      s << indent << "  Throttle[" << i << "]: ";
      Printer<float>::stream(s, indent + "  ", v.Throttle[i]);
    }
  }
};

} // namespace message_operations
} // namespace ros

#endif // FAST_DRONE_PKG_MESSAGE_CONTROLOUTPUT_H
