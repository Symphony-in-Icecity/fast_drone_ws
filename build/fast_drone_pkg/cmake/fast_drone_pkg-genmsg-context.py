# generated from genmsg/cmake/pkg-genmsg.context.in

messages_str = "/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlCommand.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/TrajectoryPoint.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Trajectory.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/AttitudeReference.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/DroneState.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/Topic_for_log.msg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg/ControlOutput.msg"
services_str = ""
pkg_name = "fast_drone_pkg"
dependencies_str = "geometry_msgs;sensor_msgs;std_msgs"
langs = "gencpp;geneus;genlisp;gennodejs;genpy"
dep_include_paths_str = "fast_drone_pkg;/home/dqn/fast_drone_ws/src/fast_drone_pkg/msg;geometry_msgs;/opt/ros/kinetic/share/geometry_msgs/cmake/../msg;sensor_msgs;/opt/ros/kinetic/share/sensor_msgs/cmake/../msg;std_msgs;/opt/ros/kinetic/share/std_msgs/cmake/../msg"
PYTHON_EXECUTABLE = "/usr/bin/python2"
package_has_static_sources = '' == 'TRUE'
genmsg_check_deps_script = "/opt/ros/kinetic/share/genmsg/cmake/../../../lib/genmsg/genmsg_check_deps.py"
