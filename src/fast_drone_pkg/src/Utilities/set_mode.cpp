/***************************************************************************************************************************
* set_mode.cpp
*
* Author: SFL
*
* Update Time: 2019.3.16
*
* Introduction:  Change mode
                单独运行用来测试锁定解锁，设置模式之类的是否正确.
***************************************************************************************************************************/

//头文件
#include <ros/ros.h>


#include <iostream>

//话题头文件
#include <tcp_server.h>


using namespace std;

// uint8_t radioMsg[32]={0x0a, 0x0a ,0x13 ,0x00 ,0x25 ,0x00, 0x34 ,0x11 ,0x44 ,0x11, 0x55 ,0xdc, 0x65 ,0xdc,0x75 ,0xdc,0x85 ,0xdc, 0x95 ,0xdc, 0xa4 ,0x11 ,0xb4 ,0x11, 0xc4, 0x11 ,0xd4 ,0x11 ,0xe4 ,0x11 ,0xf4, 0x11};


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>主 函 数<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
int main(int argc, char **argv)
{
    ros::init(argc, argv, "set_mode");
    ros::NodeHandle nh("~");
    command_to_drone _command_to_drone;
    // int serial_open = serial_init();

    //1.创建一个socket
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == -1)
    {
        cout << "socket 创建失败： "<< endl;
        exit(1);
    }
    //2.准备通讯地址（必须是服务器的）192.168.35.103是本机的IP
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(8888);//将一个无符号短整型的主机数值转换为网络字节顺序，即大尾顺序(big-endian)
    addr.sin_addr.s_addr = inet_addr("192.168.35.103");//net_addr方法可以转化字符串，主要用来将一个十进制的数转化为二进制的数，用途多于ipv4的IP转化。
    //3.bind()绑定
    //参数一：0的返回值（socket_fd）
    //参数二：(struct sockaddr*)&addr 前面结构体，即地址
    //参数三: addr结构体的长度
    int res = bind(socket_fd,(struct sockaddr*)&addr,sizeof(addr));
    if (res == -1)
    {
        cout << "bind创建失败： " << endl;
        exit(-1);
    }
    cout << "bind ok 等待客户端的连接" << endl;
    //4.监听客户端listen()函数
    //参数二：进程上限，一般小于30,同时能监听多少个客户端
    listen(socket_fd,30);
    //5.等待客户端的连接accept()，返回用于交互的socket描述符
    struct sockaddr_in client;
    socklen_t len = sizeof(client);
    int fd = accept(socket_fd,(struct sockaddr*)&client,&len);
    if (fd == -1)
    {
        cout << "accept错误\n" << endl;
        exit(-1);
    }
    //6.使用第5步返回socket描述符，进行读写通信。
    char *ip = inet_ntoa(client.sin_addr);
    cout << "客户： 【" << ip << "】连接成功" << endl;



    ros::Rate rate(100);

    int Num_StateMachine = 0;
    int flag_1;

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>主  循  环<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
    while(ros::ok())
    {
        switch (Num_StateMachine)
        {
            // input
            case 0:
                cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>--------<<<<<<<<<<<<<<<<<<<<<<<<<<< "<< endl;
                cout << "Input the mode:  0 for ACRO, 1 for ANGLE, 2 for HORIZON, 3 for disarm, 4 for arm "<<endl;
                cin >> flag_1;

                //1000 降落 也可以指向其他任务
                if (flag_1 == 0)
                {
                    Num_StateMachine = 1;
                    break;
                }
                else if (flag_1 == 1)
                {
                    Num_StateMachine = 2;
                    break;
                }
                else if(flag_1 == 2)
                {
                    Num_StateMachine = 3;
                }//惯性系移动
                else if(flag_1 == 3)
                {
                    Num_StateMachine = 4;
                }
                else if(flag_1 == 4)
                {
                    Num_StateMachine = 5;
                }

                break;

        //ACRO
        case 1:
            Num_StateMachine = 0;
            break;

        //STABILIZED
        case 2:
            Num_StateMachine = 0;
            break;

        //HORIZON
        case 3:
            Num_StateMachine = 0;
            break;

        //disarm
        case 4:
            _command_to_drone.arm_disarm(false);
            Num_StateMachine = 0;
            break;

        //arm
        case 5:
            _command_to_drone.arm_disarm(true);
            Num_StateMachine = 0;
            break;

        }

        write(fd, radioMsg,32);

        int size = read(fd, buffer, sizeof(buffer));//通过fd与客户端联系在一起,返回接收到的字节数
        //第一个参数：accept 返回的文件描述符
        //第二个参数：存放读取的内容
        //第三个参数：内容的大小
        if (size != 0)
        {
            cout << "接收到字节数为： " << size << endl;
            cout << "内容： "<< buffer << endl;

            for (int i = 0; i < size; i++)
            {
                printf("%d ",(uint8_t)buffer[i]);
            }
            cout << endl;
        }
        else
        {
            cout << "-----未接收到客户端数据，可能连接已经断开-----" << endl;
            cout << "-----正在重新建立连接-----" << endl;
            close(fd);

            fd = accept(socket_fd,(struct sockaddr*)&client,&len);
            if (fd == -1)
            {
                cout << "accept错误\n" << endl;
                exit(-1);
            }
            char *ip = inet_ntoa(client.sin_addr);
            cout << "客户： 【" << ip << "】连接成功" << endl;
        }
        //周期休眠
        // ser.write(radioMsg,32);
        rate.sleep();
    }
    //7.关闭sockfd
    close(fd);
    close(socket_fd);
    return 0;
}
