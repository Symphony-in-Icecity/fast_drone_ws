(cl:in-package fast_drone_pkg-msg)
(cl:export '(HEADER-VAL
          HEADER
          COMMAND_ID-VAL
          COMMAND_ID
          MODE-VAL
          MODE
          REFERENCE_STATE-VAL
          REFERENCE_STATE
))