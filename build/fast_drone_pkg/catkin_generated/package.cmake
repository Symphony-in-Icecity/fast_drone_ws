set(_CATKIN_CURRENT_PACKAGE "fast_drone_pkg")
set(fast_drone_pkg_VERSION "0.0.0")
set(fast_drone_pkg_MAINTAINER "lihai <lihai@todo.todo>")
set(fast_drone_pkg_PACKAGE_FORMAT "2")
set(fast_drone_pkg_BUILD_DEPENDS "message_generation" "std_msgs")
set(fast_drone_pkg_BUILD_EXPORT_DEPENDS "std_msgs")
set(fast_drone_pkg_BUILDTOOL_DEPENDS "catkin")
set(fast_drone_pkg_BUILDTOOL_EXPORT_DEPENDS )
set(fast_drone_pkg_EXEC_DEPENDS "message_runtime" "std_msgs")
set(fast_drone_pkg_RUN_DEPENDS "message_runtime" "std_msgs")
set(fast_drone_pkg_TEST_DEPENDS )
set(fast_drone_pkg_DOC_DEPENDS )
set(fast_drone_pkg_URL_WEBSITE "")
set(fast_drone_pkg_URL_BUGTRACKER "")
set(fast_drone_pkg_URL_REPOSITORY "")
set(fast_drone_pkg_DEPRECATED "")