/***************************************************************************************************************************
* drone_pos_controller.cpp
*
* Author: SFL
*
* Update Time: 2020.9.9
*
* Introduction:  Fast Drone Position Controller 
*         1. 从应用层节点订阅/fast_drone/control_command话题（ControlCommand.msg），接收来自上层的控制指令。
*         2. 从pos_estimator读取无人机的状态信息（DroneState.msg）。
*         3. 调用位置环控制算法，计算加速度控制量。可选择cascade_PID, PID, UDE, passivity-UDE, NE+UDE位置控制算法。目前使用串级pid
*         4. 通过网络将控制指令发到ESP8266，具体的ESP8266由YSC封装
*         5. 发送相关信息至地面站节点(/fast_drone_pkg/groud_log)，供监控使用。
***************************************************************************************************************************/



#include <ros/ros.h>
#include <Eigen/Eigen>

#include <fast_drone_pkg/AttitudeReference.h>
#include <fast_drone_pkg/ControlCommand.h>
#include <fast_drone_pkg/DroneState.h>
#include <fast_drone_pkg/Topic_for_log.h>
#include <pos_controller_cascade_PID.h>
#include <fast_drone_pkg/ControlOutput.h>
#include <command_to_drone.h>
#include <circle_trajectory.h>
#include <eight_trajectory.h>
#include <tcp_server.h>

using namespace std;


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>变量声明<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
fast_drone_pkg::ControlCommand Command_Now;                      //无人机当前执行命令
fast_drone_pkg::ControlCommand Command_Last;                     //无人机上一条执行命令
fast_drone_pkg::ControlCommand Command_to_gs;                    //发送至地面站的指令
fast_drone_pkg::DroneState _DroneState;                          //无人机状态量
Eigen::Vector3d throttle_sp;
fast_drone_pkg::ControlOutput _ControlOutput;
fast_drone_pkg::AttitudeReference _AttitudeReference;           //位置控制器输出，即姿态环参考量
fast_drone_pkg::Topic_for_log _Topic_for_log;                  //用于日志记录的topic
float cur_time;

float Takeoff_height;                                       //起飞高度
float Disarm_height;                                        //自动上锁高度

//Geigraphical fence 地理围栏
Eigen::Vector2f geo_fence_x;
Eigen::Vector2f geo_fence_y;
Eigen::Vector2f geo_fence_z;
//起飞的位置
Eigen::Vector3d Takeoff_position = Eigen::Vector3d(0.0,0.0,0.0);
// uint8_t map_last[10]={0x00 , 0x02 , 0x04 , 0x06 , 0x07 , 0x08, 0x0a , 0x0c , 0x0e , 0x0f };

// uint8_t radioMsg[32]={0x0a, 0xff, 0x05, 0xdc, 0x05, 0xdc, 0x03, 0xea, 0x05, 0xdc, 0x06, 0xdc, 0x06, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc,};
// uint8_t temp_variable[32]={0x0a, 0xff, 0x05, 0xdc, 0x05, 0xdc, 0x03, 0xea, 0x05, 0xdc, 0x06, 0xdc, 0x06, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc, 0x05, 0xdc,};


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>函数声明<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
void printf_param();
int check_failsafe();
float get_time_in_sec(const ros::Time& begin_time);
// uint8_t* bf_sbus_trans(uint8_t* source_msg, uint8_t* temp_variable );
fast_drone_pkg::AttitudeReference ThrottleToAttitude(const Eigen::Vector3d& thr_sp, float yaw_sp);



void drone_state_cb(const fast_drone_pkg::DroneState::ConstPtr& msg)
{
    _DroneState = *msg;

    _DroneState.time_from_start = cur_time;
}

void Command_cb(const fast_drone_pkg::ControlCommand::ConstPtr& msg)
{
    Command_Now = *msg;
    
    // 无人机一旦接受到Land指令，则会屏蔽其他指令
    if(Command_Last.Mode == command_to_drone::Land)
    {
        Command_Now.Mode = command_to_drone::Land;
    }

    // 检测电子围栏，超过电子围栏的话就自动降落
    if(check_failsafe() == 1)
    {
        Command_Now.Mode = command_to_drone::Land;
    }
}


int main(int argc, char *argv[])
{
    ros::init(argc, argv, "drone_pos_controller");
    ros::NodeHandle nh("~");

    //【订阅】无人机当前状态
    // 本话题来自根据需求自定drone_pos_estimator.cpp
    ros::Subscriber drone_state_sub = nh.subscribe<fast_drone_pkg::DroneState>("/fast_drone/drone_state", 10, drone_state_cb);

    //【订阅】指令
    // 本话题来自根据需求自定义的上层模块，比如track_circle.cpp 比如move.cpp
    ros::Subscriber Command_sub = nh.subscribe<fast_drone_pkg::ControlCommand>("/fast_drone/control_command", 10, Command_cb);

    // 【发布】日志
    // 发布log消息至ground_station.cpp
    ros::Publisher log_pub = nh.advertise<fast_drone_pkg::Topic_for_log>("/fast_drone/topic_for_log", 10);

    // 参数读取
    nh.param<float>("Takeoff_height", Takeoff_height, 1.0);
    nh.param<float>("Disarm_height", Disarm_height, 0.15);
    // nh.param<float>("Use_accel", Use_accel, 0.0);
    // nh.param<int>("Flag_printf", Flag_printf, 0.0);
    
    nh.param<float>("geo_fence/x_min", geo_fence_x[0], -10.0);
    nh.param<float>("geo_fence/x_max", geo_fence_x[1], 10.0);
    nh.param<float>("geo_fence/y_min", geo_fence_y[0], -10.0);
    nh.param<float>("geo_fence/y_max", geo_fence_y[1], 10.0);
    nh.param<float>("geo_fence/z_min", geo_fence_z[0], -10.0);
    nh.param<float>("geo_fence/z_max", geo_fence_z[1], 10.0);


    // 位置控制选取为50Hz，主要取决于位置状态的更新频率
    ros::Rate rate(100);

    command_to_drone _command_to_drone;
    // 位置控制类 - 串级pid
    pos_controller_cascade_PID pos_controller_cascade_pid;
    pos_controller_cascade_pid.printf_param();
    Circle_Trajectory _Circle_Trajectory;
    Eight_Trajectory _Eight_Trajectory;
    float time_trajectory = 0.0;
    printf_param();

    int check_flag;
    // 这一步是为了程序运行前检查一下参数是否正确
    // 输入1,继续，其他，退出程序
    cout << "Please check the parameter and setting，enter 1 to continue， else for quit: "<<endl;
    cin >> check_flag;

    if(check_flag != 1)
    {
        return -1;
    }

    // 先读取一些飞控的数据
    for(int i=0; i<50; i++)
    {
        ros::spinOnce();
        rate.sleep();
    }

    // Set the takeoff position
    Takeoff_position[0] = _DroneState.position[0];
    Takeoff_position[1] = _DroneState.position[1];
    Takeoff_position[2] = _DroneState.position[2];

    // 初始化命令-
    // 默认设置：Idle模式 电机怠速旋转 等待来自上层的控制指令
    Command_Now.Mode = command_to_drone::Idle;
    Command_Now.Command_ID = 0;
    Command_Now.Reference_State.Sub_mode  = command_to_drone::XYZ_POS;
    Command_Now.Reference_State.position_ref[0] = 0;
    Command_Now.Reference_State.position_ref[1] = 0;
    Command_Now.Reference_State.position_ref[2] = 0;
    Command_Now.Reference_State.velocity_ref[0] = 0;
    Command_Now.Reference_State.velocity_ref[1] = 0;
    Command_Now.Reference_State.velocity_ref[2] = 0;
    Command_Now.Reference_State.acceleration_ref[0] = 0;
    Command_Now.Reference_State.acceleration_ref[1] = 0;
    Command_Now.Reference_State.acceleration_ref[2] = 0;
    Command_Now.Reference_State.yaw_ref = 0;

    //1.创建一个socket
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == -1)
    {
        cout << "socket 创建失败： "<< endl;
        exit(1);
    }
    //2.准备通讯地址（必须是服务器的）192.168.35.103是本机的IP
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(8888);//将一个无符号短整型的主机数值转换为网络字节顺序，即大尾顺序(big-endian)
    addr.sin_addr.s_addr = inet_addr("192.168.2.101");//net_addr方法可以转化字符串，主要用来将一个十进制的数转化为二进制的数，用途多于ipv4的IP转化。
    //3.bind()绑定
    //参数一：0的返回值（socket_fd）
    //参数二：(struct sockaddr*)&addr 前面结构体，即地址
    //参数三: addr结构体的长度
    int res = bind(socket_fd,(struct sockaddr*)&addr,sizeof(addr));
    if (res == -1)
    {
        cout << "bind创建失败： " << endl;
        exit(-1);
    }
    cout << "bind ok 等待客户端的连接" << endl;
    //4.监听客户端listen()函数
    //参数二：进程上限，一般小于30,同时能监听多少个客户端
    listen(socket_fd,30);
    //5.等待客户端的连接accept()，返回用于交互的socket描述符
    struct sockaddr_in client;
    socklen_t len = sizeof(client);
    int fd = accept(socket_fd,(struct sockaddr*)&client,&len);
    if (fd == -1)
    {
        cout << "accept错误\n" << endl;
        exit(-1);
    }
    //6.使用第5步返回socket描述符，进行读写通信。
    char *ip = inet_ntoa(client.sin_addr);
    cout << "客户： 【" << ip << "】连接成功" << endl;

    // 记录启控时间
    ros::Time begin_time = ros::Time::now();
    float last_time = get_time_in_sec(begin_time);
    float dt = 0;

    while(ros::ok())
    {
        // 当前时间
        cur_time = get_time_in_sec(begin_time);
        dt = cur_time  - last_time;
        dt = constrain_function2(dt, 0.01, 0.03);
        last_time = cur_time;

        //执行回调函数
        ros::spinOnce();
        //状态机开启
        switch (Command_Now.Mode)
        {
            // 【Arm】 解锁
            case command_to_drone::Arm:
                _command_to_drone.arm_disarm(true);

            // 【Idle】 怠速旋转，此时可以切入offboard模式，但不会起飞。
            case command_to_drone::Idle:
                _command_to_drone.idle();
                break;
            // 【Takeoff】 从摆放初始位置原地起飞至指定高度，偏航角也保持0
            case command_to_drone::Takeoff:
                Command_to_gs.Mode = Command_Now.Mode;
                Command_to_gs.Command_ID = Command_Now.Command_ID;
                Command_to_gs.Reference_State.Sub_mode  = command_to_drone::XYZ_POS;
                Command_to_gs.Reference_State.position_ref[0] = Takeoff_position[0];
                Command_to_gs.Reference_State.position_ref[1] = Takeoff_position[1];
                Command_to_gs.Reference_State.position_ref[2] = Takeoff_height;// + Takeoff_position[2];
                Command_to_gs.Reference_State.velocity_ref[0] = 0;
                Command_to_gs.Reference_State.velocity_ref[1] = 0;
                Command_to_gs.Reference_State.velocity_ref[2] = 0;
                Command_to_gs.Reference_State.acceleration_ref[0] = 0;
                Command_to_gs.Reference_State.acceleration_ref[1] = 0;
                Command_to_gs.Reference_State.acceleration_ref[2] = 0;
                Command_to_gs.Reference_State.yaw_ref = 0; //rad

                _ControlOutput = pos_controller_cascade_pid.pos_controller(_DroneState, Command_to_gs.Reference_State, dt);
                
                throttle_sp[0] = _ControlOutput.Throttle[0];
                throttle_sp[1] = _ControlOutput.Throttle[1];
                throttle_sp[2] = _ControlOutput.Throttle[2];

                _AttitudeReference = ThrottleToAttitude(throttle_sp, Command_to_gs.Reference_State.yaw_ref);

                _command_to_drone.send_attitude_setpoint(_AttitudeReference);   

                break;
            // 【Move_ENU】 ENU系移动。
            case command_to_drone::Move_ENU:
                Command_to_gs = Command_Now;

                _ControlOutput = pos_controller_cascade_pid.pos_controller(_DroneState, Command_to_gs.Reference_State, dt);

                throttle_sp[0] = _ControlOutput.Throttle[0];
                throttle_sp[1] = _ControlOutput.Throttle[1];
                throttle_sp[2] = _ControlOutput.Throttle[2];

                _AttitudeReference = ThrottleToAttitude(throttle_sp, Command_to_gs.Reference_State.yaw_ref);

                _command_to_drone.send_attitude_setpoint(_AttitudeReference);

                break;
            // 【Move_Body】 机体系移动。
            case command_to_drone::Move_Body:
                //还没写好
                break;
            // 【Hold】 悬停。当前位置悬停
            case command_to_drone::Hold:
                Command_to_gs.Mode = Command_Now.Mode;
                Command_to_gs.Command_ID = Command_Now.Command_ID;
                if (Command_Last.Mode != command_to_drone::Hold)
                {
                    Command_to_gs.Reference_State.Sub_mode  = command_to_drone::XYZ_POS;
                    Command_to_gs.Reference_State.position_ref[0] = _DroneState.position[0];
                    Command_to_gs.Reference_State.position_ref[1] = _DroneState.position[1];
                    Command_to_gs.Reference_State.position_ref[2] = _DroneState.position[2];
                    Command_to_gs.Reference_State.velocity_ref[0] = 0;
                    Command_to_gs.Reference_State.velocity_ref[1] = 0;
                    Command_to_gs.Reference_State.velocity_ref[2] = 0;
                    Command_to_gs.Reference_State.acceleration_ref[0] = 0;
                    Command_to_gs.Reference_State.acceleration_ref[1] = 0;
                    Command_to_gs.Reference_State.acceleration_ref[2] = 0;
                    Command_to_gs.Reference_State.yaw_ref = _DroneState.attitude[2]; //rad
                }
                break;
            // 【Land】 降落。当前位置原地降落，降落后会自动上锁
            case command_to_drone::Land:
                Command_to_gs.Mode = Command_Now.Mode;
                Command_to_gs.Command_ID = Command_Now.Command_ID;
                if (Command_Last.Mode != command_to_drone::Land)
                {
                    Command_to_gs.Reference_State.Sub_mode  = command_to_drone::XYZ_POS;
                    Command_to_gs.Reference_State.position_ref[0] = _DroneState.position[0];
                    Command_to_gs.Reference_State.position_ref[1] = _DroneState.position[1];
                    Command_to_gs.Reference_State.position_ref[2] = Takeoff_position[2];
                    Command_to_gs.Reference_State.velocity_ref[0] = 0;
                    Command_to_gs.Reference_State.velocity_ref[1] = 0;
                    Command_to_gs.Reference_State.velocity_ref[2] = 0;
                    Command_to_gs.Reference_State.acceleration_ref[0] = 0;
                    Command_to_gs.Reference_State.acceleration_ref[1] = 0;
                    Command_to_gs.Reference_State.acceleration_ref[2] = 0;
                    Command_to_gs.Reference_State.yaw_ref = _DroneState.attitude[2]; //rad
                }

                //如果距离起飞高度小于10厘米，则直接上锁或者切换成怠速模式；
                if(abs(_DroneState.position[2] - Takeoff_position[2]) < Disarm_height)
                {
                    Command_Now.Mode = command_to_drone::Disarm;
                }else
                {
                    _ControlOutput = pos_controller_cascade_pid.pos_controller(_DroneState, Command_to_gs.Reference_State, dt);

                    throttle_sp[0] = _ControlOutput.Throttle[0];
                    throttle_sp[1] = _ControlOutput.Throttle[1];
                    throttle_sp[2] = _ControlOutput.Throttle[2];

                    _AttitudeReference = ThrottleToAttitude(throttle_sp, Command_to_gs.Reference_State.yaw_ref);

                    _command_to_drone.send_attitude_setpoint(_AttitudeReference);
                }

                break;
            // 【Disarm】 紧急上锁。直接上锁，不建议使用，危险。
            case command_to_drone::Disarm:
                _command_to_drone.arm_disarm(false);
                break;
            // Circle_Tracking 轨迹追踪控制，与上述追踪点或者追踪速度不同，此时期望输入为一段轨迹
            case command_to_drone::Circle_Tracking:
                //还没写好
                Command_to_gs.Mode = Command_Now.Mode;
                Command_to_gs.Command_ID = Command_Now.Command_ID;
                if (Command_Last.Mode != command_to_drone::Circle_Tracking)
                {
                    time_trajectory = 0.0;
                }

                time_trajectory = time_trajectory + dt;

                Command_to_gs.Reference_State = _Circle_Trajectory.Circle_trajectory_generation(time_trajectory);

                _ControlOutput = pos_controller_cascade_pid.pos_controller(_DroneState, Command_to_gs.Reference_State, dt);

                throttle_sp[0] = _ControlOutput.Throttle[0];
                throttle_sp[1] = _ControlOutput.Throttle[1];
                throttle_sp[2] = _ControlOutput.Throttle[2];

                _AttitudeReference = ThrottleToAttitude(throttle_sp, Command_to_gs.Reference_State.yaw_ref);

                _command_to_drone.send_attitude_setpoint(_AttitudeReference);
                // Quit  悬停于最后一个目标点
                if (time_trajectory >= _Circle_Trajectory.time_total)
                {
                    Command_Now.Mode = command_to_drone::Move_ENU;
                    Command_Now.Reference_State = Command_to_gs.Reference_State;
                }
                break;
            // Eight_Tracking 轨迹追踪控制，与上述追踪点或者追踪速度不同，此时期望输入为一段8字轨迹
            case command_to_drone::Eight_Tracking:
                Command_to_gs.Mode = Command_Now.Mode;
                Command_to_gs.Command_ID = Command_Now.Command_ID;
                
                if (Command_Last.Mode != command_to_drone::Eight_Tracking)
                {
                    time_trajectory = 0.0;
                }

                time_trajectory = time_trajectory + dt;

                Command_to_gs.Reference_State = _Eight_Trajectory.Eight_trajectory_generation(time_trajectory);

                _ControlOutput = pos_controller_cascade_pid.pos_controller(_DroneState, Command_to_gs.Reference_State, dt);

                throttle_sp[0] = _ControlOutput.Throttle[0];
                throttle_sp[1] = _ControlOutput.Throttle[1];
                throttle_sp[2] = _ControlOutput.Throttle[2];

                _AttitudeReference = ThrottleToAttitude(throttle_sp, Command_to_gs.Reference_State.yaw_ref);

                _command_to_drone.send_attitude_setpoint(_AttitudeReference);

                // Quit  悬停于最后一个目标点
                if (time_trajectory >= _Eight_Trajectory.time_total)
                {
                    Command_Now.Mode = command_to_drone::Move_ENU;
                    Command_Now.Reference_State = Command_to_gs.Reference_State;
                }
                break;
        }

        // For log
        if(cur_time == 0)
        {
            _Topic_for_log.time = -1.0;
        }
        else
        {
            _Topic_for_log.time = cur_time;
        }

        _Topic_for_log.header.stamp = ros::Time::now();
        _Topic_for_log.Drone_State = _DroneState;
        _Topic_for_log.Control_Command = Command_to_gs;
        _Topic_for_log.Attitude_Reference = _AttitudeReference;
        _Topic_for_log.Control_Output = _ControlOutput;

        log_pub.publish(_Topic_for_log);

        Command_Last = Command_Now;

        // ser.write(bf_sbus_trans(radioMsg,temp_variable),32);
        write(fd, radioMsg,32);

        int size = read(fd, buffer, sizeof(buffer));//通过fd与客户端联系在一起,返回接收到的字节数
        //第一个参数：accept 返回的文件描述符
        //第二个参数：存放读取的内容
        //第三个参数：内容的大小
        if (size != 0)
        {
            cout << "接收到字节数为： " << size << endl;
            cout << "内容： "<< buffer << endl;

            for (int i = 0; i < size; i++)
            {
                printf("%d ",(uint8_t)buffer[i]);
            }
            cout << endl;
        }
        else
        {
            cout << "-----未接收到客户端数据，可能连接已经断开-----" << endl;
            cout << "-----正在重新建立连接-----" << endl;
            close(fd);

            fd = accept(socket_fd,(struct sockaddr*)&client,&len);
            if (fd == -1)
            {
                cout << "accept错误\n" << endl;
                exit(-1);
            }
            char *ip = inet_ntoa(client.sin_addr);
            cout << "客户： 【" << ip << "】连接成功" << endl;
        }
        rate.sleep();
    }

    //7.关闭sockfd
    close(fd);
    close(socket_fd);
    
    return 0;
}


// uint8_t* bf_sbus_trans(uint8_t* source_msg, uint8_t* temp_variable)
// {
//     uint8_t* target_msg = temp_variable;
//     target_msg[0] = 0x0a;
//     target_msg[1] = 0xff;
//     uint16_t outl,outh,channels,output;
//     for(int i=1;i<8;i++)
//     {
//         channels=(uint16_t)(source_msg[2*i]<<8)|source_msg[2*i+1];
//         outh=(channels-1000)/160;
//         outl=(channels-1000-160*outh)/10*16;
//         outl+=map_last[channels%10];
//         output =(outh<<8)|outl+0xc0;
//         target_msg[2*i+1]=(uint8_t)(output&0x00ff);
//         target_msg[2*i] = (uint8_t)( output>>8);
//     }
//     return target_msg;
// }

void printf_param()
{
    cout <<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Parameter <<<<<<<<<<<<<<<<<<<<<<<<<<<" <<endl;
    cout << "Takeoff_height: "<< Takeoff_height<<" [m] "<<endl;
    cout << "Disarm_height : "<< Disarm_height <<" [m] "<<endl;
    cout << "geo_fence_x : "<< geo_fence_x[0] << " [m]  to  "<<geo_fence_x[1] << " [m]"<< endl;
    cout << "geo_fence_y : "<< geo_fence_y[0] << " [m]  to  "<<geo_fence_y[1] << " [m]"<< endl;
    cout << "geo_fence_z : "<< geo_fence_z[0] << " [m]  to  "<<geo_fence_z[1] << " [m]"<< endl;
    
}


int check_failsafe()
{
    if (_DroneState.position[0] < geo_fence_x[0] || _DroneState.position[0] > geo_fence_x[1] ||
        _DroneState.position[1] < geo_fence_y[0] || _DroneState.position[1] > geo_fence_y[1] ||
        _DroneState.position[2] < geo_fence_z[0] || _DroneState.position[2] > geo_fence_z[1])
    {
        return 1;
        cout << "Out of the geo fence, the drone is landing... "<< endl;
    }
    else{
        return 0;
    }
}


// 【获取当前时间函数】 单位：秒
float get_time_in_sec(const ros::Time& begin_time)
{
    ros::Time time_now = ros::Time::now();
    float currTimeSec = time_now.sec-begin_time.sec;
    float currTimenSec = time_now.nsec / 1e9 - begin_time.nsec / 1e9;
    return (currTimeSec + currTimenSec);
}


fast_drone_pkg::AttitudeReference ThrottleToAttitude(const Eigen::Vector3d& thr_sp, float yaw_sp)
{
    fast_drone_pkg::AttitudeReference _AttitudeReference;
    Eigen::Vector3d att_sp;
    att_sp[2] = yaw_sp;

    // desired body_z axis = -normalize(thrust_vector)
    Eigen::Vector3d body_x, body_y, body_z;

    double thr_sp_length = thr_sp.norm();

    //cout << "thr_sp_length : "<< thr_sp_length << endl;

    if (thr_sp_length > 0.00001f) {
            body_z = thr_sp.normalized();

    } else {
            // no thrust, set Z axis to safe value
            body_z = Eigen::Vector3d(0.0f, 0.0f, 1.0f);
    }

    // vector of desired yaw direction in XY plane, rotated by PI/2
    Eigen::Vector3d y_C = Eigen::Vector3d(-sinf(yaw_sp),cosf(yaw_sp),0.0);

    if (fabsf(body_z(2)) > 0.000001f) {
            // desired body_x axis, orthogonal to body_z
            body_x = y_C.cross(body_z);

            // keep nose to front while inverted upside down
            if (body_z(2) < 0.0f) {
                    body_x = -body_x;
            }

            body_x.normalize();

    } else {
            // desired thrust is in XY plane, set X downside to construct correct matrix,
            // but yaw component will not be used actually
            body_x = Eigen::Vector3d(0.0f, 0.0f, 0.0f);
            body_x(2) = 1.0f;
    }

    // desired body_y axis
    body_y = body_z.cross(body_x);

    Eigen::Matrix3d R_sp;

    // fill rotation matrix
    for (int i = 0; i < 3; i++) {
            R_sp(i, 0) = body_x(i);
            R_sp(i, 1) = body_y(i);
            R_sp(i, 2) = body_z(i);
    }

    Eigen::Quaterniond q_sp(R_sp);

    rotation_to_euler(R_sp, att_sp);

    //cout << "Desired euler [R P Y]: "<< att_sp[0]* 180/M_PI <<" [deg] " << att_sp[1]* 180/M_PI <<" [deg] "<< att_sp[2]* 180/M_PI <<" [deg] "<< endl;
    //cout << "Desired Thrust: "<< thr_sp_length<< endl;
//    cout << "q_sp [x y z w]: "<< q_sp.x() <<" [ ] " << q_sp.y() <<" [ ] "<<q_sp.z() <<" [ ] "<<q_sp.w() <<" [ ] "<<endl;
//    cout << "R_sp : "<< R_sp(0, 0) <<" " << R_sp(0, 1) <<" "<< R_sp(0, 2) << endl;
//    cout << "     : "<< R_sp(1, 0) <<" " << R_sp(1, 1) <<" "<< R_sp(1, 2) << endl;
//    cout << "     : "<< R_sp(2, 0) <<" " << R_sp(2, 1) <<" "<< R_sp(2, 2) << endl;


    _AttitudeReference.throttle_sp[0] = thr_sp[0];
    _AttitudeReference.throttle_sp[1] = thr_sp[1];
    _AttitudeReference.throttle_sp[2] = thr_sp[2];

    //期望油门
    _AttitudeReference.desired_throttle = thr_sp_length; 

    _AttitudeReference.desired_att_q.w = q_sp.w();
    _AttitudeReference.desired_att_q.x = q_sp.x();
    _AttitudeReference.desired_att_q.y = q_sp.y();
    _AttitudeReference.desired_att_q.z = q_sp.z();

    _AttitudeReference.desired_attitude[0] = att_sp[0];  
    _AttitudeReference.desired_attitude[1] = att_sp[1]; 
    _AttitudeReference.desired_attitude[2] = att_sp[2]; 

    return _AttitudeReference;
}













