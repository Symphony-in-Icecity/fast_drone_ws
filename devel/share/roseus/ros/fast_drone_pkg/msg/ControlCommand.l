;; Auto-generated. Do not edit!


(when (boundp 'fast_drone_pkg::ControlCommand)
  (if (not (find-package "FAST_DRONE_PKG"))
    (make-package "FAST_DRONE_PKG"))
  (shadow 'ControlCommand (find-package "FAST_DRONE_PKG")))
(unless (find-package "FAST_DRONE_PKG::CONTROLCOMMAND")
  (make-package "FAST_DRONE_PKG::CONTROLCOMMAND"))

(in-package "ROS")
;;//! \htmlinclude ControlCommand.msg.html
(if (not (find-package "STD_MSGS"))
  (ros::roseus-add-msgs "std_msgs"))


(intern "*IDLE*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*IDLE* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*IDLE* 0)
(intern "*TAKEOFF*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*TAKEOFF* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*TAKEOFF* 1)
(intern "*MOVE_ENU*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*MOVE_ENU* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*MOVE_ENU* 2)
(intern "*MOVE_BODY*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*MOVE_BODY* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*MOVE_BODY* 3)
(intern "*HOLD*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*HOLD* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*HOLD* 4)
(intern "*LAND*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*LAND* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*LAND* 5)
(intern "*DISARM*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*DISARM* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*DISARM* 6)
(intern "*EIGHT_TRACKING*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*EIGHT_TRACKING* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*EIGHT_TRACKING* 7)
(intern "*CIRCLE_TRACKING*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*CIRCLE_TRACKING* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*CIRCLE_TRACKING* 8)
(intern "*ARM*" (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(shadow '*ARM* (find-package "FAST_DRONE_PKG::CONTROLCOMMAND"))
(defconstant fast_drone_pkg::ControlCommand::*ARM* 9)
(defclass fast_drone_pkg::ControlCommand
  :super ros::object
  :slots (_header _Command_ID _Mode _Reference_State ))

(defmethod fast_drone_pkg::ControlCommand
  (:init
   (&key
    ((:header __header) (instance std_msgs::Header :init))
    ((:Command_ID __Command_ID) 0)
    ((:Mode __Mode) 0)
    ((:Reference_State __Reference_State) (instance fast_drone_pkg::TrajectoryPoint :init))
    )
   (send-super :init)
   (setq _header __header)
   (setq _Command_ID (round __Command_ID))
   (setq _Mode (round __Mode))
   (setq _Reference_State __Reference_State)
   self)
  (:header
   (&rest __header)
   (if (keywordp (car __header))
       (send* _header __header)
     (progn
       (if __header (setq _header (car __header)))
       _header)))
  (:Command_ID
   (&optional __Command_ID)
   (if __Command_ID (setq _Command_ID __Command_ID)) _Command_ID)
  (:Mode
   (&optional __Mode)
   (if __Mode (setq _Mode __Mode)) _Mode)
  (:Reference_State
   (&rest __Reference_State)
   (if (keywordp (car __Reference_State))
       (send* _Reference_State __Reference_State)
     (progn
       (if __Reference_State (setq _Reference_State (car __Reference_State)))
       _Reference_State)))
  (:serialization-length
   ()
   (+
    ;; std_msgs/Header _header
    (send _header :serialization-length)
    ;; uint32 _Command_ID
    4
    ;; uint8 _Mode
    1
    ;; fast_drone_pkg/TrajectoryPoint _Reference_State
    (send _Reference_State :serialization-length)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; std_msgs/Header _header
       (send _header :serialize s)
     ;; uint32 _Command_ID
       (write-long _Command_ID s)
     ;; uint8 _Mode
       (write-byte _Mode s)
     ;; fast_drone_pkg/TrajectoryPoint _Reference_State
       (send _Reference_State :serialize s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; std_msgs/Header _header
     (send _header :deserialize buf ptr-) (incf ptr- (send _header :serialization-length))
   ;; uint32 _Command_ID
     (setq _Command_ID (sys::peek buf ptr- :integer)) (incf ptr- 4)
   ;; uint8 _Mode
     (setq _Mode (sys::peek buf ptr- :char)) (incf ptr- 1)
   ;; fast_drone_pkg/TrajectoryPoint _Reference_State
     (send _Reference_State :deserialize buf ptr-) (incf ptr- (send _Reference_State :serialization-length))
   ;;
   self)
  )

(setf (get fast_drone_pkg::ControlCommand :md5sum-) "c74d2afcd25f5819ed8a889d1832c173")
(setf (get fast_drone_pkg::ControlCommand :datatype-) "fast_drone_pkg/ControlCommand")
(setf (get fast_drone_pkg::ControlCommand :definition-)
      "std_msgs/Header header

## 控制命令的编号 防止接收到错误命令， 编号应该逐次递加
uint32 Command_ID

## 控制命令的模式 
uint8 Mode
# enum Mode 控制模式枚举
uint8 Idle=0
uint8 Takeoff=1
uint8 Move_ENU=2
uint8 Move_Body=3
uint8 Hold=4
uint8 Land=5
uint8 Disarm=6
uint8 Eight_Tracking=7
uint8 Circle_Tracking=8
uint8 Arm=9

## 控制参考量 
## 位置参考量：位置、速度、加速度、加加速度、加加加速度
## 角度参考量：偏航角、偏航角速度、偏航角加速度
TrajectoryPoint Reference_State

================================================================================
MSG: std_msgs/Header
# Standard metadata for higher-level stamped data types.
# This is generally used to communicate timestamped data 
# in a particular coordinate frame.
# 
# sequence ID: consecutively increasing ID 
uint32 seq
#Two-integer timestamp that is expressed as:
# * stamp.sec: seconds (stamp_secs) since epoch (in Python the variable is called 'secs')
# * stamp.nsec: nanoseconds since stamp_secs (in Python the variable is called 'nsecs')
# time-handling sugar is provided by the client library
time stamp
#Frame this data is associated with
# 0: no frame
# 1: global frame
string frame_id

================================================================================
MSG: fast_drone_pkg/TrajectoryPoint
std_msgs/Header header

## 时刻： 用于轨迹追踪
float32 time_from_start          ## [s]

# sub_mode 2-bit value:
# 0 for position, 1 for vel, 1st for xy, 2nd for z.
#                   xy position     xy velocity
# z position       	0b00(0)       0b10(2)
# z velocity		0b01(1)       0b11(3)
#
## 控制命令的子模式 
## 可以选择 位置追踪、速度追踪或者其他复合模式，
## 默认为 XYZ位置追踪模式 （sub_mode = 0）； 速度追踪启用时，控制器不考虑位置参考量及位置状态反馈
uint8 Sub_mode
# enum sub_mode
uint8 XYZ_POS = 0
uint8 XY_POS_Z_VEL = 1
uint8 XY_VEL_Z_POS = 2
uint8 XY_VEL_Z_VEL = 3

## 参考量：位置、速度、加速度、加加速度、加加加速度
float32[3] position_ref          ## [m]
float32[3] velocity_ref          ## [m/s]
float32[3] acceleration_ref      ## [m/s^2]
## float32[3] jerk_ref              ## [m/s^3]
## float32[3] snap_ref              ## [m/s^4]

## 角度参考量：偏航角、偏航角速度、偏航角加速度
float32 yaw_ref                  ## [rad]
## float32 yaw_rate_ref             ## [rad/s] 
## float32 yaw_acceleration_ref     ## [rad/s] 
")



(provide :fast_drone_pkg/ControlCommand "c74d2afcd25f5819ed8a889d1832c173")


