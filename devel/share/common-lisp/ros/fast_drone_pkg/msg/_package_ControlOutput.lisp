(cl:in-package fast_drone_pkg-msg)
(cl:export '(HEADER-VAL
          HEADER
          U_L-VAL
          U_L
          U_D-VAL
          U_D
          NE-VAL
          NE
          V_CMD-VAL
          V_CMD
          THRUST-VAL
          THRUST
          THROTTLE-VAL
          THROTTLE
))